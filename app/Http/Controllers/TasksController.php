<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json(Task::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        if (!$request->input('title')) {
            abort(400);
        }
        $task = new Task([
            'title' => $request->input('title'),
            'done' => false,
        ]);
        return response()->json($task->save());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        if (!$task) {
            abort(404);
        }
        return response()->json($task->delete());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function toggle($id)
    {
        $task = Task::find($id);
        if (!$task) {
            abort(404);
        }
        return response()->json(
            $task->update([
                'done' => !$task->done
            ])
        );
    }
}
